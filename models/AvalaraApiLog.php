<?php

namespace popglobal\avalara\models;

use popglobal\log\models\BaseLog;
use yii;
use yii\db\ActiveRecord;
use yii\db\Exception;

/**
 * This is the model class for table "avalara_api_log".
 *
 * @property integer $id
 * @property string  $url
 * @property integer $method
 * @property boolean $error
 * @property integer $request_data
 * @property integer $response_data
 */
class AvalaraApiLog extends BaseLog {
    const METHOD_GET = 1;
    const METHOD_POST = 2;

    public static $methods = [
        self::METHOD_GET  => 'GET',
        self::METHOD_POST => 'POST',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'avalara_api_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['url', 'method'], 'required'],
            [['method'], 'in', 'range' => array_keys(self::$methods)],
            [['request_data', 'response_data'], 'string'],
        ];
    }

    /**
     * @param array $data
     * @return bool
     * @throws Exception
     */
    public static function add(array $data = [])
    {
        $data['method'] = array_search($data['method'], self::$methods);

        return parent::add($data);
    }
}
