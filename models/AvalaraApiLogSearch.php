<?php
namespace popglobal\avalara\models;

use yii\data\ActiveDataProvider;
use popglobal\avalara\models\AvalaraApiLog;

/**
 * AvalaraApiLogSearch represents the model behind the search form about AvalaraApiLog
 */
class AvalaraApiLogSearch extends AvalaraApiLog
{
    public $date_from;
    public $date_to;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['url', 'method', 'date_from', 'date_to'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AvalaraApiLog::find()->orderBy(['id' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider(['query' => $query]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'method' => $this->method,
        ]);

        if (!empty($this->url)) {
            $query->andFilterWhere(['like', 'url', $this->url]);
        }

        if (!empty($this->date_from)) {
            $query->andWhere('created_at >= :created_from',
                [':created_from' => strtotime($this->date_from . ' 00:00:00')]);
        }

        if (!empty($this->date_to)) {
            $query->andWhere('created_at <= :created_to',
                [':created_to' => strtotime($this->date_to . ' 23:59:59')]);
        }

        return $dataProvider;
    }
}
