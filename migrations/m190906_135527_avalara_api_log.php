<?php
use yii\db\Migration;

/**
 * Class m190906_135527_avalara_api_log
 */
class m190906_135527_avalara_api_log extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('avalara_api_log', [
            'id' => $this->bigPrimaryKey()->unsigned(),
            'created_at' => $this->int(11),
            'url' => $this->string()->notNull(),
            'method' => $this->tinyInteger()->unsigned()->notNull(),
            'error' => $this->boolean(),
            'request_data' => $this->string(1024),
            'response_data' => $this->string(1024),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->createIndex('date_time', 'avalara_api_log', ['date_time']);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('avalara_api_log');
    }
}
