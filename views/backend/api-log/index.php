<?php
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel avalara\models\AvalaraApiLogSearch */

$this->title = 'Avalara API Request Log';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="avalara-request-log-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<div class="filters">
		<?php echo $this->render('_search', ['model' => $searchModel]); ?>
	</div>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'columns' => [
			[
				'header' => 'Date, Action',
				'attribute' => 'created_at',
				'value' => function ($model) {
					$data = '<div class="text-center">';
                    $data .= date('Y-m-d H:i:s', $model->created_at) . ' &nbsp;';
					$data .= $model->method . '<br/>' . str_replace(['http://', '/', '?', '&'], ['', ' /', ' ?', ' &'], $model->url);
					$data .= '</div>';
					return $data;
				},
				'contentOptions' => ['style' => 'width:20%; white-space: normal; font-size: 12px;'],
				'format' => 'html',
			],
			[
				'attribute' => 'request',
				'value' => function ($model) {
					return !empty($model->request_data) ? str_replace([':',','], [': ',', '], $model->request_data) : '-';
				},
				'contentOptions' => ['style' => 'width:40%; white-space: normal; font-size: 12px;'],
			],
			[
				'attribute' => 'response',
				'value' => function ($model) {
                    return !empty($model->response_data) ? str_replace([':',','], [': ',', '], $model->response_data) : '-';
				},
                'contentOptions' => ['style' => 'width:40%; white-space: normal; font-size: 12px;'],
			],
		],
	]); ?>
</div>
