<?php
namespace popglobal\avalara\components\sdk\definition;

/**
 * Class ItemTax
 * @package popglobal\avalara\components\sdk\definition
 */
class ItemTax extends BaseDefinition
{
    public $bill;
    public $cmpl;
    public $tm;
    public $calc;
    public $cat;
    public $cid;
    public $name;
    public $exm;
    public $lns;
    public $min;
    public $pcd;
    public $rate;
    public $sur;
    public $tax;
    public $lvl;
    public $tid;
    public $err;
}
