<?php
namespace popglobal\avalara\components\sdk\definition;

/**
 * Class Item
 * @package popglobal\avalara\components\sdk\definition
 */
class Item extends BaseDefinition
{
    public $ref;
    public $base;
    public $txs;

    public function __construct(array $item_data = [])
    {
        parent::__construct($item_data);

        if (!empty($item_data['txs'])) {
            $this->txs = [];
            foreach ($item_data['txs'] as $item_tax_data) {
                $this->txs[] = new ItemTax($item_tax_data);
            }
        }
    }
}
