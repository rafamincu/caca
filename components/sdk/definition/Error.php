<?php
namespace popglobal\avalara\components\sdk\definition;

/**
 * Class Error
 * @package popglobal\avalara\components\sdk\definition
 */
class Error extends BaseDefinition
{
    public $code;
    public $msg;
}
