<?php
namespace popglobal\avalara\components\sdk\response;

use popglobal\avalara\components\sdk\definition\Invoice;

/**
 * Class CalcTaxesResponse
 * @package popglobal\avalara\components\sdk\response
 */
class CalcTaxesResponse extends BaseResponse
{
    public $inv = [];

    public function __construct(array $response_data = [])
    {
        parent::__construct($response_data);

        if (!empty($response_data['inv'])) {
            $this->inv = [];
            foreach ($response_data['inv'] as $invoice_data) {
                $this->inv[] = new Invoice($invoice_data);
            }
        }
    }
}