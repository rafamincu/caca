<?php
namespace popglobal\avalara\components\sdk\response;

use popglobal\avalara\components\sdk\definition\BaseDefinition;
use popglobal\avalara\components\sdk\definition\Error;

/**
 * Class BaseResponse
 * @package avalo\components\sdk\response
 */
class BaseResponse extends BaseDefinition
{
    public function __construct(array $response_data = [])
    {
        parent::__construct($response_data);
    }
}

