<?php
namespace popglobal\avalara\controllers\backend;

use yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

use popglobal\avalara\models\AvalaraApiLog;
use popglobal\avalara\models\AvalaraApiLogSearch;

/**
 * ApiLogController implements the log view for AvalaraApiLog model.
 */
class ApiLogController extends Controller
{
    /**
     * Lists all Logs.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AvalaraApiLogSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Finds the AvalaraApiLog model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AvalaraApiLog the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AvalaraApiLog::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested resource does not exist.');
        }
    }
}
